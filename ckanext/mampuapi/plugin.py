import pylons.config as config
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit


import psycopg2


#@toolkit.side_effect_free
def get_user_api_key(context,data_dict=None):
    #get user api key by user name
    dbname = config.get('ckan.mampuapi.dbname')
    dbhost = config.get('ckan.mampuapi.host')
    dbuser =  config.get('ckan.mampuapi.user')
    dbpassword =  config.get('ckan.mampuapi.password')

    try:
        conn = psycopg2.connect("dbname="+ dbname +" user="+ dbuser +" host="+ dbhost +" password=" + dbpassword)
        cur = conn.cursor()
        cur.execute("select apikey from public.user where name='" + data_dict['name'] + "'")
        rows = cur.fetchone()
    except psycopg2.Error as e:
        return {'success':False,
                'msg': e.pgerror}

    return {'res':rows}



class MampuapiPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.interfaces.IActions)
    # IConfigurer


    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'mampuapi')

    def get_actions(self):
        return {'get_user_api_key':get_user_api_key}


